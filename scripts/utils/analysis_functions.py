import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime

################
# Fit function #
################

def ivCurveFit(I, Vin, minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = 'tab:blue', RextReference = -1, label = ''):
    subScriptStr  = ''
    if label:
        subScriptStr = '_' + label
    conditionAnd  = np.logical_and(Vin > minVoltageVal, Vin < maxVoltageVal)
    conditionAnd  = np.logical_and(conditionAnd, I > minCurrentVal)
    conditionAnd  = np.logical_and(conditionAnd, I < maxCurrentVal)
    IShort        = I[conditionAnd]
    VinShort      = Vin[conditionAnd]
    m,q           = np.polyfit(IShort,VinShort,1)
    abline_values = [m * v + q for v in I]
    ivLinePlt  ,  = plt.plot(I     , abline_values, '--', color = lineColor , label = 'IV Fit'          )
    ivShortPlt ,  = plt.plot(IShort, VinShort     , 'x' , color = 'black'   , label = 'Fit Points'  )
    if RextReference > 0:
        textstr = '\n'.join((
            r'$m' + subScriptStr + '=%.3f$'     % (m, ),
            r'$q' + subScriptStr + '=%.3f$'     % (q, ),
            r'$R^{ext}' + subScriptStr + '=%d$' % (RextReference,)
            ))
    else:
        textstr = '\n'.join((
            r'$m' + subScriptStr + '=%.3f$'     % (m, ),
            r'$q' + subScriptStr + '=%.3f$'     % (q, )
            ))

    return ivLinePlt, ivShortPlt, textstr

###################
# OVP Calculation #
###################

def ovp_v_calculate(label, textstr, valueList, currentOVP, voltageOVP, minVoltageVal, minCurrentVal, I, VinDid, VinAid, resultWriter, resultLine):
    """Calculate over-protection voltage and current
    """
    VinAorDid = VinDid if (label == "D") else VinAid

    resultSlope = textstr.split('$')[1].split('=')[1]

    for i in range(0,len(valueList[VinAorDid]) - 1):
        if (valueList[VinAid][i] > minVoltageVal and I[i] > minCurrentVal):
            increase = (valueList[VinAorDid][i+1] - valueList[VinAorDid][i]) / (I[i+1] - I[i])
            if (increase < 0.8 * float(resultSlope)):
                print("OVP started above Vin{}[V]: ".format(label), valueList[VinAorDid][i], " and I[A]: ", I[i])
                currentOVP = I[i]
                voltageOVP = valueList[VinAorDid][i]
                if (resultWriter):
                    if (label == "D"):
                        resultLine.append(I[i])
                    resultLine.append(valueList[VinAorDid][i])
                break
    if (currentOVP == -1):
        print("OVP-{} not found".format(label))

    return currentOVP,voltageOVP,resultLine

##################
# Bool Converter #
##################

def str_to_bool(s):
    if s == 'true' or s =='True':
         return True
    elif s == 'false' or s=='False':
         return False
    else:
         raise ValueError

##################
# POSIX TS Class #
##################

class posixTS():
    def __init__(self,dateandtime):
        self.dateandtime = dateandtime
    
    def getTS(self):
        date            = datetime(
                year    = int(self.dateandtime[0]) ,
                month   = int(self.dateandtime[1]) ,
                day     = int(self.dateandtime[2]) ,
                hour    = int(self.dateandtime[3]) ,
                minute  = int(self.dateandtime[4]) ,
                second  = int(self.dateandtime[5])
                )
        timeStamp       = datetime.timestamp(date)
        return timeStamp

###################
# Debugging print #
###################

def debuggingPrint(string, variable, verbosityValue, verbosityLevel = 0):
    if (verbosityValue > verbosityLevel):
        print('\n'+string+'\n')
        print(variable)
