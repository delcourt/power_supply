/*!
 * \author Stefan Maier <s.maier@kit.edu>, ETP-Karlsruhe
 * \author Andrea Rizzi
 */
#ifndef RIGOLDP800_H
#define RIGOLDP800_H

#include "PowerSupply.h"
#include "PowerSupplyChannel.h"

class Connection;

class RigolDP800 : public PowerSupply
{
  public:
    RigolDP800(const pugi::xml_node configuration);
    virtual ~RigolDP800();
    void configure();
    // bool turnOn();
    // bool turnOff();

  private:
    Connection* fConnection;
};

class RigolDP800Channel : public PowerSupplyChannel
{
  public:
    RigolDP800Channel(Connection* connection, const pugi::xml_node configuration);
    virtual ~RigolDP800Channel();

    // bool  reset() override;
    // bool  isOpen() override;
    void turnOn(void) override;
    void turnOff(void) override;
    bool isOn(void) override;
    // bool  setVoltageMode() override;
    // bool  setCurrentMode() override;
    // bool  setVoltageRange(float voltage) override;
    // bool  setCurrentRange(float current) override;
    void setVoltage(float voltage) override;
    void setCurrent(float current) override;
    void setVoltageCompliance(float voltage) override;
    void setCurrentCompliance(float current) override;
    void setOverVoltageProtection(float voltage) override;
    void setOverCurrentProtection(float current) override;

    float getSetVoltage() override;
    float getOutputVoltage() override;
    float getCurrent() override;
    float getVoltageCompliance() override;
    float getCurrentCompliance() override;
    float getOverVoltageProtection() override;
    float getOverCurrentProtection() override;
    void  setParameter(std::string parName, float value) override;
    void  setParameter(std::string parName, bool value) override;
    void  setParameter(std::string parName, int value) override;
    float getParameterFloat(std::string parName) override;
    int   getParameterInt(std::string parName) override;
    bool  getParameterBool(std::string parName) override;

  private:
    Connection* fConnection;
    std::string fChannel;

    void        write(std::string);
    std::string read(std::string);
};

#endif
